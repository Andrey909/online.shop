@extends('template')

@section('content')

    @foreach($pages as $page)

        <div class="col-md-4">
            <h2>{{ $page['title'] }}</h2>
            <p> {{ $page['intro'] }} </p>
            <p><a class="btn btn-primary" href="/pages/{{ $page['alias'] }}" role="button">View details »</a></p>
            @if(Auth::check())
                <p><a class="btn btn-primary" href="/pages/{{ $page['alias'] }}/edit" role="button">Edit »</a></p>
                <p>
                    <form method="post" action="/pages/{{ $page['alias'] }}">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <button class="btn btn-danger">Delete </button>
                    </form>
                </p>
            @endif
        </div>

    @endforeach

@endsection

@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Welcome to Hillel Shop</h1>
        </div>
    </div>

@endsection