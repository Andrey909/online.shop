<?php

namespace App\Http\Controllers;


use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {


        $products = Product::all();
        return view('main', compact('products'));

    }

}

