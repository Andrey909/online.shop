<?php

namespace App\Http\Controllers;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function create(){
                $cart = json_decode(request()->cookie('cart'),true);
                if(count($cart) < 1){
                        return redirect('/products');
        }
        $products = [];
        foreach ($cart as $productId =>$amount){
                        $products[] = Product::find($productId);
                    }
        return view('orders.create', compact('products','cart'));
    }

    public function store(){
            $cart = json_decode(request()->cookie('cart'),true);
            if(count($cart) < 1){
                    return redirect('/products');
        }
        $this->validate(request(),[
                'user_name' => 'required|min:3',
                'email' => 'required|email',
                'phone' => 'required|min:5'
                ]);

        $order = Order::create(request()->all());

        foreach ($cart as $productId => $productAmount){
                    $order->products()->attach($productId,['amount' => $productAmount]);
                }

        return redirect('/products')->withCookie('cart',json_encode([]));

    }
}

