<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomeController@home');


Route::resources([
        'products'=> 'ProductsController',
        'pages'=> 'PagesController']);


Route::get('/login','SessionsController@create')->name('login');
Route::post('/sessions', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

Route::get('/cart/{product}', 'CartController@store');

Route::get('/order', 'OrderController@create');
Route::post('/order', 'OrderController@store');

Route::get('/admin','admin\IndexController@index');